//
//  MapViewController.m
//  Currency exchange
//
//  Created by Admin on 11.12.15.
//  Copyright © 2015 Borsch. All rights reserved.
//

#import "MapViewController.h"
#import "exchangePointAnnotation.h"
#import <MapKit/MapKit.h>
#import "PointViewController.h"
#import "ExchangePoints.h"
#import "exchangePoint.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


@interface MapViewController ()
@property (strong, nonatomic) IBOutlet MKMapView *exchangeMap;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UISegmentedControl *currencySelector;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sellOrBuySelector;
@property (weak, nonatomic) IBOutlet UILabel *oopsLabel;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;




@end

@implementation MapViewController



//BUG: first app launch. Select "Don't allow" in the dialog asking about user location - activity indicator will spin always
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.exchangeMap.hidden = YES;
    self.oopsLabel.hidden = YES;
    self.currencySelector.hidden = YES;
    self.sellOrBuySelector.hidden = YES;
    self.updateButton.hidden = YES;
    [self.activityIndicator startAnimating];
    
    //get user current location
    //TODO: create separate class like "LocationManager" and put all related logic inside
    self.manager = [[CLLocationManager alloc] init];
    self.manager.delegate = self;
    self.manager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.manager requestWhenInUseAuthorization];
    [self.manager startUpdatingLocation];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Приложению требуется информация о геолокации"
                                                    message:@"Пожалуйста, добавьте разрешение в настройках"
                                                   delegate:self
                                          cancelButtonTitle:@"Настройки"
                                          otherButtonTitles:nil];
    [alert show];
   
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(nonnull NSArray<CLLocation *> *)locations
{
    CLLocationDistance distance = [[locations lastObject] distanceFromLocation:self.currentLocation];
    
    if (!self.currentLocation || distance > 100) {
        
        self.currentLocation = (CLLocation *)[locations lastObject];
        
        //getting and showing excange points
        [self.exchangeMap removeAnnotations:self.exchangeMap.annotations];
        NSString *urlString = [self createUrlString];
        [self getAndShowPoints:urlString];
        
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    //TODO: no coding style??
    MKAnnotationView *annView=[[MKAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"pin"];
    exchangePointAnnotation *exPointAnn = annotation;
    
    mapView.userLocation.title = @"Вы здесь";
    
    if (annView.annotation !=   mapView.userLocation)
    {

        NSString *sellBuyString = [self selectedSellOrBuy];
        NSString *currencyString = [self selectedCurrency];
        
        if ((exPointAnn.isBestSell && [sellBuyString isEqual: @"sell"])|| (exPointAnn.isBestBuy && [sellBuyString isEqual: @"buy"]))
        {
            annView.image = [UIImage imageNamed:@"pin_green.png"];
            
        }
        else
        {
            annView.image = [UIImage imageNamed:@"pin_yellow.png"];
        }

        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(6, -13, 57, 77)];
//        lbl.backgroundColor = [UIColor blackColor];
        lbl.textColor = [UIColor whiteColor];
        lbl.font = [UIFont systemFontOfSize:12];
//        lbl.alpha = 0.5;
//        lbl.textAlignment = NSTextAlignmentCenter;
        
      

        lbl.text = [NSString stringWithFormat: @"%.2f" , [exPointAnn.exPointObject.rates[currencyString][sellBuyString] doubleValue]];

        [annView addSubview:lbl];
        
        
    annView.canShowCallout = YES;
     
    annView.frame = lbl.frame;
        
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    annView.rightCalloutAccessoryView = infoButton;
        
    return annView;
    }
    
    else
    {
        return nil;
    };
    
}


- (void) mapView:(MKMapView *)map didAddAnnotationViews:(NSArray *)views
{
    for (MKAnnotationView *view in views)
    {
        if ([view annotation] !=   map.userLocation)
        {
            exchangePointAnnotation *exPointAnn = [view annotation];
            
            if (exPointAnn.isBestSell || exPointAnn.isBestBuy)
            {
                [[view superview] bringSubviewToFront:view];
            }
            else
            {
                [[view superview] sendSubviewToBack:view];
            }
        }
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    for (NSObject *annotation in [mapView annotations])
    {
        if (annotation !=   mapView.userLocation)
        {
            exchangePointAnnotation *exPointAnn = (exchangePointAnnotation *)annotation;
            MKAnnotationView *view = [mapView viewForAnnotation:exPointAnn];
            if (exPointAnn.isBestSell || exPointAnn.isBestBuy)

            {
                [[view superview] bringSubviewToFront:view];
            }
            
            else
            {
                [[view superview] sendSubviewToBack:view];
            }
        }
    }
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated

{

    for (NSObject *annotation in [mapView annotations])
    {
        if (annotation !=   mapView.userLocation)
        {
            exchangePointAnnotation *exPointAnn = (exchangePointAnnotation *)annotation;
            MKAnnotationView *view = [mapView viewForAnnotation:exPointAnn];
            if (exPointAnn.isBestSell || exPointAnn.isBestBuy)
                
            {
                [[view superview] bringSubviewToFront:view];
            }
            
            else
            {
                [[view superview] sendSubviewToBack:view];
            }
        }
    }

}

-(NSString *)createUrlString

{
    //use sugar - @(self.currentLocation.coordinate.latitude)
    NSNumber *lat = [NSNumber numberWithDouble:self.currentLocation.coordinate.latitude];
    NSNumber *lon = [NSNumber numberWithDouble:self.currentLocation.coordinate.longitude];
    NSNumber *rad = [[NSNumber alloc]initWithInt:2];//what is 2?
    
    NSString *latString = [lat stringValue];
    NSString *lonString = [lon stringValue];
    NSString *radiusString = [rad stringValue];
    
    NSString *urlString = [NSString stringWithFormat: @"http://api.cashme.co/api/v1/rates.json?point[latitude]=%@&point[longitude]=%@&radius=%@", latString, lonString, radiusString];
    
    //NSString *urlString = @"https://dl.dropboxusercontent.com/s/23cpj9t1nslb94h/rates.json?dl=0";
    //http://api.cashme.co/api/v1/rates.json?point[latitude]=55.7702012&point[longitude]=37.60247520000007&radius=1&currency=EUR
    
    return urlString;
}


- (NSString* )selectedCurrency

{
    
    return @[@"USD", @"EUR", @"GBP"][self.currencySelector.selectedSegmentIndex];
}


- (NSString* )selectedSellOrBuy

{
    
    return @[@"sell", @"buy"][self.sellOrBuySelector.selectedSegmentIndex];
    
}


- (IBAction)sellOrBuyChanged:(id)sender {
    
    
    [self.exchangeMap removeAnnotations:self.exchangeMap.annotations];

    
    [self.exchangeMap showAnnotations:self.exchangePointAnnotations animated:YES];

    
    
}



- (IBAction)currencyChanged:(id)sender {
    
    
    
    
    NSString *currencyString = [self selectedCurrency];
    
    ExchangePoints *exPoints =[ExchangePoints sharedExchangePoints];
    //find best buy rate
    int bestBuyIndex = [self getMaxIndex:exPoints currency:currencyString];
    
    
    //find best sell rate
    int bestSellIndex = [self getMinIndex:exPoints currency:currencyString];
    
    
    //create annotations
    [self createAnnotations:exPoints currency:currencyString max:bestBuyIndex  min:bestSellIndex];
    
    [self.exchangeMap removeAnnotations:self.exchangeMap.annotations];
    
    for (int i=0; i<self.exchangePointAnnotations.count; i++)
    {
        [self.exchangeMap  addAnnotation: self.exchangePointAnnotations[i]];
    }
    
    [self.exchangeMap showAnnotations:self.exchangePointAnnotations animated:YES];
    
}



- (void)getAndShowPoints:(NSString *)urlString;
{
    //view startup settings
    self.exchangeMap.hidden = YES;
    self.oopsLabel.hidden = YES;
    self.currencySelector.hidden = YES;
    self.sellOrBuySelector.hidden = YES;
    self.updateButton.hidden = YES;
    [self.activityIndicator startAnimating];
    
    //preparing URL for API

    NSString *currencyString = [self selectedCurrency];
    
    //creating request URL

    NSURL *url = [NSURL URLWithString:urlString];
    
    //TODO: remove synchronous network call in main thread
    NSString *json = [[NSString alloc] initWithContentsOfURL:url
                                                    encoding:NSUTF8StringEncoding
                                                       error:nil];
    
    if (json !=nil) {
        
        // Background thread - getting exchange points from json
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonFull = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:0
                                                                       error:nil];
            
            ExchangePoints *exPoints =[ExchangePoints sharedExchangePoints];
            NSMutableArray *jsonPoints = jsonFull[@"results"];
            [exPoints.points removeAllObjects];
            for (int i = 0; i<jsonPoints.count; i++) {
//                TODO: move logic related to data downloading and parsing into ExchangePoints class
                exchangePoint *ep = [[exchangePoint alloc]init];
                ep.bankName = jsonPoints[i][@"bank"];
                ep.pointName = jsonPoints[i][@"point"];
                ep.pointAddress = jsonPoints[i][@"address"];
                ep.rates = jsonPoints[i][@"rates"];
                ep.timetable = jsonPoints[i][@"timetable"];
                ep.coordinate = CLLocationCoordinate2DMake([jsonPoints[i][@"location"][@"latitude"] doubleValue],[jsonPoints[i][@"location"][@"longitude"] doubleValue]);
                
                [exPoints.points addObject:ep];
            }
            
            // Main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.activityIndicator stopAnimating];
                
                if (exPoints.points.count > 0)
                {
                    self.exchangeMap.hidden = NO;
                    self.currencySelector.hidden = NO;
                    self.sellOrBuySelector.hidden = NO;
                    //find best buy rate
                    int bestBuyIndex = [self getMaxIndex:exPoints currency:currencyString];
                    
                    
                    //find best sell rate
                    int bestSellIndex = [self getMinIndex:exPoints currency:currencyString];
                    
                    
                    //create annotations
                    [self createAnnotations:exPoints currency:currencyString max:bestBuyIndex  min:bestSellIndex];
                    
                    //show annotationns on the map
                    //TODO: NSInteger
                    for (int i=0; i<self.exchangePointAnnotations.count; i++)
                    {
                        [self.exchangeMap  addAnnotation: self.exchangePointAnnotations[i]];
                    }
                    
                    [self.exchangeMap showAnnotations:self.exchangePointAnnotations animated:YES];
                    
                }
                else
                {
                    self.oopsLabel.hidden = NO;
                    self.updateButton.hidden = NO;
                    [self.activityIndicator stopAnimating];
                    
                }
                
            });
        });
        
        
    }
    else
    {
        self.oopsLabel.hidden = NO;
        self.updateButton.hidden = NO;
        [self.activityIndicator stopAnimating];
    }


}

- (void)mapView:(MKMapView *)map annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control;

{
    exchangePointAnnotation *exPointAnn = view.annotation;
    [self performSegueWithIdentifier:@"PointInfo" sender:exPointAnn];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"PointInfo"]) {
        PointViewController *myVC = [segue destinationViewController];
        myVC.exPointAnnotation = sender;
      
    }
}

- (int)getMaxIndex:(ExchangePoints *)exPoints currency:(NSString *)currencyString
{
    exchangePoint *epMax = exPoints.points[0];
    double maxNumber = [epMax.rates[currencyString][@"buy"] doubleValue];
    int maxIndex = 0;
    for (int i = 0; i<exPoints.points.count; i++) {
        
        exchangePoint *ep = exPoints.points[i];
  
        if (([ep.rates isKindOfClass:[NSDictionary class]]) && (ep.rates[currencyString][@"buy"]) && ([ep.rates[currencyString][@"buy"] doubleValue] > maxNumber))
            {
                maxIndex = i;
                maxNumber = [ep.rates[currencyString][@"buy"] doubleValue];
            }
        }
    return maxIndex;
}

- (int)getMinIndex:(ExchangePoints *)exPoints currency:(NSString *)currencyString
{
    exchangePoint *epMin = exPoints.points[0];
    //TODO: CGFloat
    double minNumber = [epMin.rates[currencyString][@"sell"] doubleValue];
    
    int minIndex = 0;
    for (int i = 0; i<exPoints.points.count; i++) {
        
        exchangePoint *ep = exPoints.points[i];
        if(![ep.rates isKindOfClass:[NSDictionary class]])
        {
            NSLog(@"No, %i", i);
        }
        
      if (([ep.rates isKindOfClass:[NSDictionary class]]) && (ep.rates[currencyString][@"sell"]) && ([ep.rates[currencyString][@"sell"] doubleValue] < minNumber))
            {
                minIndex = i;
                minNumber = [ep.rates[currencyString][@"sell"] doubleValue];
            }
        }
    
    return minIndex;
}


- (void)createAnnotations:(ExchangePoints *)exPoints currency:(NSString *)currencyString max:(int)maxIndex min:(int)minIndex {
    
    //TODO: move into ExchangePoints
    self.exchangePointAnnotations = [[NSMutableArray alloc]init];
    
    for (int i=0; i<exPoints.points.count; i++)
    {
        
        if (exPoints.points[i])
        {
            exchangePointAnnotation *e = [[exchangePointAnnotation alloc] init];
        
        
            exchangePoint *ep = exPoints.points[i];
            e.exPointObject = (exchangePoint *)ep;
        
            //create annotation title
            if (([ep.rates isKindOfClass:[NSDictionary class]]) && ep.rates[currencyString][@"buy"] && ep.rates[currencyString][@"sell"] && CLLocationCoordinate2DIsValid (ep.coordinate))
            
        {
            NSString *title = [NSString stringWithFormat: @"%@ %.2f / %.2f" , currencyString,  [ep.rates[currencyString][@"buy"] doubleValue], [ep.rates[currencyString][@"sell"] doubleValue]];
        
            if (i==maxIndex)
            
            {
                e.isBestBuy = TRUE;

            }
        
            if (i==minIndex)
            
            {
                e.isBestSell = TRUE;

            }
            
            if (i==maxIndex || i==minIndex)
            {
                title = [title stringByAppendingString:@" - лучший"];
            }
        
            e.title = title;
        
        
            if (ep.rates[currencyString][@"date"])
            {
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"d.M.yyyy HH:mm:ss"];
                [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"Europe/Moscow"]];\
                NSString *strDateNow = [dateFormat stringFromDate:[NSDate date]];
                NSDate *dateNow = [dateFormat dateFromString:strDateNow];
                NSDate *dateCourceUpdated = [dateFormat dateFromString:ep.rates[currencyString][@"date"]];
//                NSLog(@"strdateNow: %@", strDateNow);
//                //        NSLog(@"dateNow: %@", dateNow);
//                NSLog(@"strdateCourceUpdated: %@", ep.rates[currencyString][@"date"]);
//                //        NSLog(@"dateCourceUpdated: %@", dateCourceUpdated);
                NSTimeInterval distanceBetweenDates = [dateNow timeIntervalSinceDate:dateCourceUpdated];
                double secondsInAnHour = 3600;
                NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
            
                if (hoursBetweenDates == 0)
                {
                    e.subtitle = [NSString stringWithFormat: @"%@ - обновлен только что", ep.bankName];
                }
                else
                {
                    e.subtitle = [NSString stringWithFormat: @"%@ - обновлен %ld ч. назад", ep.bankName, (long)hoursBetweenDates];
                }
            }
            else
            {
                e.subtitle = [NSString stringWithFormat: @"%@", ep.bankName];
            }
        //create location
            e.coordinate =  ep.coordinate;
            [self.exchangePointAnnotations addObject:e];
        }
        }
        
    }
    
}


- (IBAction)updateButtonTapped:(id)sender {


    //creating request URL
    NSString *urlString = [self createUrlString];
    
    //getting and showing excange points
    [self getAndShowPoints:urlString];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
