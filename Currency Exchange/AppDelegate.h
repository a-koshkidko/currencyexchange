//
//  AppDelegate.h
//  Currency exchange
//
//  Created by Admin on 09.12.15.
//  Copyright © 2015 Borsch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;



@end

