//
//  exchangePointAnnotation.h
//  Currency exchange
//
//  Created by Admin on 11.12.15.
//  Copyright © 2015 Borsch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "exchangePoint.h"

@interface exchangePointAnnotation : NSObject<MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;//write assign explicitly
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic) BOOL isBestSell;//write assign explicitly
@property (nonatomic) BOOL isBestBuy;//write assign explicitly
@property (nonatomic, strong) exchangePoint *exPointObject;

@end


