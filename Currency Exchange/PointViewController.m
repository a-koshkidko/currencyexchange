//
//  PointViewController.m
//  Currency Exchange
//
//  Created by Admin on 17.12.15.
//  Copyright © 2015 Alexey Koshkidko. All rights reserved.
//

#import "PointViewController.h"
#import "ExchangePoints.h"
#import "exchangePoint.h"
#import "exchangePointAnnotation.h"


@interface PointViewController ()
@property (strong, nonatomic) IBOutlet UILabel *bankName;
@property (strong, nonatomic) IBOutlet UILabel *pointName;
@property (strong, nonatomic) IBOutlet UILabel *timeTable;

@property (strong, nonatomic) IBOutlet UILabel *rateUSD;
@property (strong, nonatomic) IBOutlet UILabel *rateEUR;
@property (strong, nonatomic) IBOutlet UILabel *rateGBP;
@property (strong, nonatomic) IBOutlet UILabel *address;
@property (strong, nonatomic) IBOutlet MKMapView *pointMap;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;






@end

@implementation PointViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.scroll.frame = self.view.bounds;
    self.scroll.contentSize = CGSizeMake(320,1000);//TODO: investigate ios sdk
    
    if (self.exPointAnnotation.exPointObject.bankName)
    {
        self.bankName.text = self.exPointAnnotation.exPointObject.bankName;
    }
    
    if (self.exPointAnnotation.exPointObject.pointName)
    {
        self.pointName.text = self.exPointAnnotation.exPointObject.pointName;
    }
    
    if (self.exPointAnnotation.exPointObject.pointAddress)
    {
        self.address.text = [self.address.text stringByAppendingString: self.exPointAnnotation.exPointObject.pointAddress];
    }
    else
    {
        self.address.text = [self.address.text stringByAppendingString: @"Нет информации"];
    }

    
    if (self.exPointAnnotation.exPointObject.timetable)
    {
        self.timeTable.text = [self.timeTable.text  stringByAppendingString: self.exPointAnnotation.exPointObject.timetable];
    }
    else
    {
        self.timeTable.text = [self.timeTable.text  stringByAppendingString: @"Нет информации"];
    }

        
    if (self.exPointAnnotation.exPointObject.rates[@"USD"])
    {
        self.rateUSD.text = [self.rateUSD.text stringByAppendingString:[NSString stringWithFormat: @"%.2f / %.2f",[self.exPointAnnotation.exPointObject.rates[@"USD"][@"buy"] doubleValue], [self.exPointAnnotation.exPointObject.rates[@"USD"][@"sell"] doubleValue]]];
    }
    else
    {
        self.rateUSD.text = [self.rateUSD.text  stringByAppendingString: @"Нет информации"];
    }
    
    if (self.exPointAnnotation.exPointObject.rates[@"EUR"])
    {
        self.rateEUR.text =  [self.rateEUR.text stringByAppendingString:[NSString stringWithFormat: @"%.2f / %.2f",  [self.exPointAnnotation.exPointObject.rates[@"EUR"][@"buy"] doubleValue], [self.exPointAnnotation.exPointObject.rates[@"EUR"][@"sell"] doubleValue]]];
    }
    else
    {
        self.rateEUR.text = [self.rateEUR.text  stringByAppendingString: @"Нет информации"];
    }
    
   if (self.exPointAnnotation.exPointObject.rates[@"GBP"])
   {
    self.rateGBP.text = [self.rateGBP.text stringByAppendingString:[NSString stringWithFormat: @"%.2f / %.2f",  [self.exPointAnnotation.exPointObject.rates[@"GBP"][@"buy"] doubleValue], [self.exPointAnnotation.exPointObject.rates[@"GBP"][@"sell"] doubleValue]]];
   }
   else
   {
       self.rateGBP.text = [self.rateGBP.text  stringByAppendingString: @"Нет информации"];
   }

    
    if (self.exPointAnnotation)
    {
        [self.pointMap  addAnnotation: self.exPointAnnotation];
        [self.pointMap showAnnotations:self.pointMap.annotations animated:YES];
    }
    
    
//    self.scrollView.contentSize = CGSizeMake(320, 1000);
   
//    self.scrollView.frame = self.view.bounds;
//    self.scrollView.contentSize = CGSizeMake(320, 1000);

//    CGSize constraint = CGSizeMake(self.pointDescription.bounds.size.height, NSUIntegerMax);
//    
//    //    NSDictionary *attributes = @{NSFontAttributeName: font};
//    
//    CGRect rect = [self.cityDescriptionLabel.text boundingRectWithSize:constraint
//                                                               options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
//                                                            attributes: @{NSFontAttributeName: self.cityDescriptionLabel.font}
//                                                               context:nil];
//    
//    
//    
//    
//    UIScrollView *scrollView = [[UIScrollView alloc] init];
//    scrollView.frame = self.view.bounds;
//    scrollView.contentSize = CGSizeMake(320, rect.size.height + 190);
//    //CGSizeMake(320, 1000);  (self.cityDescriptionLabel.frame.size.width, self.cityDescriptionLabel.frame.size.height);
//    
//    
//    
//    
//    [scrollView addSubview:self.cityNameLabel];
//    [scrollView addSubview:self.countryNameLabel];
//    [scrollView addSubview:self.cityDescriptionLabel];
//    [self.view addSubview:scrollView];


}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    //TODO:hm, have seen it somewhere...
    MKPinAnnotationView *annView=[[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"pin"];
    exchangePointAnnotation *exPointAnn = annotation;
    
    mapView.userLocation.title = @"Вы здесь";
    
    if (annView.annotation !=   mapView.userLocation)
    {
        if (exPointAnn.isBestSell || exPointAnn.isBestBuy) {
            [annView setPinColor: MKPinAnnotationColorGreen];
        }
        else
        {
            [annView setPinColor: MKPinAnnotationColorPurple];
        }
        
        return annView;
    }
    
    else
    {
        return nil;
    };
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
