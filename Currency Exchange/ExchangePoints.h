//
//  ExchangePoints.h
//  Currency Exchange
//
//  Created by Admin on 16.12.15.
//  Copyright © 2015 Alexey Koshkidko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "exchangePoint.h"

@interface ExchangePoints : NSObject {

     NSMutableArray *points;
}

@property (nonatomic, retain) NSMutableArray *points;

+ (id)sharedExchangePoints;


@end