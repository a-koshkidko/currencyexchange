//
//  ExchangePoints.m
//  Currency Exchange
//
//  Created by Admin on 16.12.15.
//  Copyright © 2015 Alexey Koshkidko. All rights reserved.
//

#import "ExchangePoints.h"

@implementation ExchangePoints

@synthesize points;

#pragma mark Singleton Methods

+ (id)sharedExchangePoints {
    static ExchangePoints *sharedExchangePoints = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedExchangePoints = [[self alloc] init];
    });
    return sharedExchangePoints;
}

- (id)init {
    if (self = [super init]) {
        points = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

@end
