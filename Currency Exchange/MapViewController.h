//
//  MapViewController.h
//  Currency exchange
//
//  Created by Admin on 11.12.15.
//  Copyright © 2015 Borsch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController<CLLocationManagerDelegate, MKMapViewDelegate>
@property (strong, nonatomic) NSMutableArray *exchangePointAnnotations;
@property (strong, nonatomic) NSMutableArray *exchangePoints;
@property (strong, nonatomic) CLLocationManager *manager;
@property (strong, nonatomic) CLLocation *currentLocation;

@end
