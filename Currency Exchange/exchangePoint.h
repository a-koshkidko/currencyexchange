//
//  exchangePoint.h
//  Currency exchange
//
//  Created by Admin on 11.12.15.
//  Copyright © 2015 Borsch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface exchangePoint : NSObject//use library for json mapping - https://github.com/icanzilb/JSONModel https://github.com/EasyMapping/EasyMapping
@property (nonatomic, copy) NSString *bankName;//usually strong instead of copy. Read about differences
@property (nonatomic, copy) NSString *pointName;
@property (nonatomic, copy) NSString *pointAddress;
@property (nonatomic, copy) NSString *timetable;
@property (nonatomic, copy) NSDictionary *rates;//use custom object
@property (nonatomic) CLLocationCoordinate2D coordinate;//write assign explicitly
@property (nonatomic, copy) NSString *currency;

//
//"bank":"БайкалБанк",
//"point":"Операционный офис № 1",
//"timetable":"пн.—пт.: 09:30—17:30 перерыв: 14:00—15:00",
//"address":"г. Москва, Каретный ряд, д. 5/10, стр. 2",
//"rate":{
//    "buy":69.07,
//    "sell":71.61,
//    "date":"20.10.2015 09:42:00",
//    "buy_change":0,
//    "sell_change":0
//},
//"location":{
//    "latitude":55.772466,
//    "longitude":37.609008
//}

@end
