//
//  PointViewController.h
//  Currency Exchange
//
//  Created by Admin on 17.12.15.
//  Copyright © 2015 Alexey Koshkidko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "exchangePointAnnotation.h"

@interface PointViewController : UIViewController
@property (strong,nonatomic) exchangePointAnnotation *exPointAnnotation;

@end
